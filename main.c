#include "egg-line.h"

static EggLine *line = NULL;

static EggLineCommand* channel_iter (EggLine *line, gint *argc, gchar ***argv);

static EggLineCommand commands[] =
{
	{ "channel", channel_iter, NULL, NULL, NULL },
	{ "source", NULL, NULL, NULL, NULL },
	{ NULL }
};

static EggLineCommand channel_commands[] =
{
	{ "add", NULL, NULL, NULL, NULL },
	{ "edit", NULL, NULL, NULL, NULL },
	{ "remove", NULL, NULL, NULL, NULL },
	{ NULL }
};

static EggLineCommand*
channel_iter (EggLine   *line,
              gint      *argc,
              gchar   ***argv)
{
	return channel_commands;
}

static void
test_EggLine_resolve (void)
{
	gchar **argv = NULL;
	gint    argc = 0;

	g_assert (egg_line_resolve (line, "channel ", NULL, NULL) == &commands [0]);
	g_assert (egg_line_resolve (line, "source  ", NULL, NULL) == &commands [1]);
	g_assert (egg_line_resolve (line, "channel add ", NULL, NULL) == &channel_commands [0]);
	g_assert (egg_line_resolve (line, "channel remove ", NULL, NULL) == &channel_commands [2]);
	g_assert (egg_line_resolve (line, "chan", NULL, NULL) == NULL);

	g_assert (egg_line_resolve (line, "channel add name chris", &argc, &argv) == &channel_commands [0]);
	g_assert_cmpint (argc, ==, 2);
	g_assert_cmpstr (argv[0], ==, "name");
	g_assert_cmpstr (argv[1], ==, "chris");
}

gint
main (gint   argc,
      gchar *argv[])
{
	line = egg_line_new ();
	egg_line_set_prompt (line, "egg> ");
	egg_line_set_commands (line, commands);

	g_test_init (&argc, &argv, NULL);

	g_test_add_func ("/EggLine/resolve", test_EggLine_resolve);

	return g_test_run ();
}
