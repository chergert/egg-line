all: egg-line

egg-line: egg-line.c egg-line.h main.c
	$(CC) -g -o $@ -Wall -Werror egg-line.c main.c `pkg-config --libs --cflags gobject-2.0` -lreadline

clean:
	rm -rf egg-line
